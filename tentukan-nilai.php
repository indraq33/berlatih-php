<!DOCTYPE html>
<html>
<head>
	<title>Tentukan Nilai</title>
</head>
<body>
	<?php
			/*
			86 - 100 sangat baik
			70 - 85 baik
			60 - 70 cukup
			59 kurang
			*/
			function tentukan_nilai($number)
			{
			    if ($number <= 100 && $number >=86) {
			    	echo "Sangat Baik <br>";
			    } 
			    elseif ($number <= 85 && $number >= 70) {
			    	echo "Baik  <br>";
			    }
			    elseif ($number <= 69 && $number >=60 ) {
			    	echo "Cukup <br>";
			    } 
			    elseif ($number <= 59) {
			    	echo "Kurang <br>";
			    }
			}

			//TEST CASES
			echo tentukan_nilai(98); //Sangat Baik
			echo tentukan_nilai(76); //Baik
			echo tentukan_nilai(67); //Cukup
			echo tentukan_nilai(43); //Kurang
	?>
</body>
</html>